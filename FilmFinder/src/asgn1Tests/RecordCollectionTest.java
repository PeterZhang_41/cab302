package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Index.IndexException;
import asgn1Index.Record;
import asgn1Index.RecordCollection;

public class RecordCollectionTest {
	
	private RecordCollection tester;
	private Record tester_record;

	@Before
	public void set_up() throws IndexException{
		tester = new RecordCollection();
		tester_record = new Record("Batman", 100);
	}
	
	@Test(expected=IndexException.class)
	public void exception_addRecord() throws IndexException{
		tester.addRecord(null);
	}
	
	@Test
	public void normal_addRecord() throws IndexException{
		tester.addRecord(tester_record);
		
	}
	
	@Test(expected = IndexException.class)
	public void empty_addRecord() throws IndexException{

		tester.addRecord(new Record("",100));
	}
	
	@Test(expected =IndexException.class)
	public void add_two_sameRecords() throws IndexException{
		tester.addRecord(new Record("Batman",100));
		tester.addRecord(new Record("Batman",100));
		tester.findClosestRecords(2);	
	}

	
	@Test
	public void normal_isSorted(){
		assertEquals(false,tester.isSorted());
		tester.sortCollection();
		assertEquals(true,tester.isSorted());	
	}
	
	@Test
	public void added_notSorted() throws IndexException{
		tester.addRecord(new Record("Fast7",100));
		assertFalse(tester.isSorted());
		
	}
	
	@Test(expected=IndexException.class)
	public void exception_findClosestRecord_noSorted() throws IndexException{
		tester.findClosestRecord();
	}
	
	@Test(expected=IndexException.class)
	public void expection_findClosestRecord_empty() throws IndexException{
		tester.sortCollection();
		tester.findClosestRecord();
		
	}
	
	@Test(expected=IndexException.class)
	public void expection_findClosestRecords_noSorted() throws IndexException{
		tester.addRecord(new Record("Fast7",100));
		tester.addRecord(new Record("Fast6",99));
		tester.findClosestRecords(2);
	}
	
	@Test(expected=IndexException.class)
	public void expection_findClosestRecords_exceed() throws IndexException{
		tester.addRecord(new Record("Fast7",100));
		tester.addRecord(new Record("Fast6",99));
		tester.sortCollection();
		tester.findClosestRecords(3);
	}
	
	@Test
	public void normal_findClosestRecord() throws IndexException{
		tester.addRecord(new Record("Fast6",99));
		tester.addRecord(new Record("Fast7",100));
		tester.addRecord(new Record("Fast1",0));
		tester.sortCollection();
		Record temp = new Record("Fast7",100);
		assertEquals(temp.toString(),tester.findClosestRecord().toString());
	}
	
	@Test
	public void normal_findClosestRecords() throws IndexException{
		tester.addRecord(new Record("Fast6",99));
		tester.addRecord(new Record("Fast7",100));
		tester.addRecord(new Record("Fast1",0));
		tester.addRecord(new Record("Fast2",88));
		tester.sortCollection();
		assertEquals("[[Fast7:100], [Fast6:99]]",tester.findClosestRecords(2).toString());	
	}
	
	@Test
	public void empty_findClosestRecords() throws IndexException{
		tester.sortCollection();
		assertEquals("[]",tester.findClosestRecords(0).toString());
	}
	
	

}
