package asgn1Tests;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import asgn1Collection.Listing;
import asgn1Collection.MovieCollection;
import asgn1Collection.MovieListing;
import asgn1Collection.ListingException;

import org.junit.Before;
import org.junit.Test;

public class MovieListingTest {
	
	private MovieListing tester;
	private MovieCollection mc;
	private BitSet bs;
	private Map<String,Listing> listings;

	@Before
	public void setup() throws ListingException, FileNotFoundException, IOException{
		
		tester = new MovieListing("Casablanca",1942);
		mc = new MovieCollection();
		
		mc.getCollectionFromFile("testdata//keyTest.txt");
		mc.generateKeyVectors();
		bs = new BitSet(mc.numKeywords());
		listings = mc.getListing();
		//Casablanca: {4,6,9,10,13,16}
		bs.set(4);bs.set(6);bs.set(9);bs.set(10);bs.set(13);bs.set(16);
		
	}	
	
	@Test
	public void normal_constructor() throws ListingException{
		assertEquals("Casablanca",tester.getTitle());
		assertEquals(1942,tester.getYear());	
	}
	
	
	@Test(expected = ListingException.class)
	public void exception_constructor_null() throws ListingException{
		try{
			tester= new MovieListing(null, 1991);}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = ListingException.class)
	public void exception_constructor_yearNegative() throws ListingException{
		try{
			tester= new MovieListing("test_string", -1);}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = ListingException.class)
	public void exception_constructor_yearZero() throws ListingException{
		try{
			tester= new MovieListing("test_string", 0);}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = ListingException.class)
	public void exception_constructor_empty() throws ListingException{
		try{
			tester= new MovieListing("", 1991);}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	public void normal_addKeyword() throws ListingException{
		tester.addKeyword("test_string");
		for (String words:tester.getKeywords()){
		assertEquals("test_string",words);	
		}
	}
	
	@Test 
	public void duplicate_addKeyword() throws ListingException{
		
		tester.addKeyword("test_string");
		tester.addKeyword("test_string");
		assertTrue(1==tester.numKeywords());	
		assertEquals("[test_string]",tester.getKeywords().toString());	
	}
	
	@Test
	public void add_two_keywords() throws ListingException{
		tester.addKeyword("test_string_1");
		tester.addKeyword("test_string_2");
		assertEquals(2,tester.numKeywords());
		assertTrue(tester.getKeywords().contains("test_string_1"));
		assertTrue(tester.getKeywords().contains("test_string_2"));
	}
	
	
	@Test(expected = ListingException.class)
	public void exception_addKeyword_null() throws ListingException{
		String null_string=null;
		try{
			tester.addKeyword(null_string);
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = ListingException.class)
	public void exception_addKeyword_empty() throws ListingException{
		String empty_string="";
		try{
			tester.addKeyword(empty_string);
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	public void normal_findsimilarity() throws ListingException{
		tester.setKeyVector(bs);		
		assertEquals(6,tester.findSimilarity(listings.get("CASABLANCA")));	
	}
	
	@Test
	public void EmptyVector_Parameter_findSimilarity() throws ListingException{
		tester.setKeyVector(bs);
		MovieListing tester_empty = new MovieListing("Casablanca",1942);
		BitSet empty = new BitSet(1);
		tester_empty.setKeyVector(empty);
		assertEquals(0,tester.findSimilarity(tester_empty));
	}
	
	@Test
	public void EmptyVector_self_findSimilarity() throws ListingException{
		BitSet empty = new BitSet(1);
		tester.setKeyVector(empty);
		MovieListing temp= new MovieListing("Casablanca",1942);
		temp.setKeyVector(bs);
		assertEquals(0,tester.findSimilarity(temp));
	}
	
	@Test(expected=ListingException.class)
	public void NullVector_Parameter_findSimilarity() throws ListingException{
		BitSet none = null;
		tester.setKeyVector(bs);
		MovieListing temp= new MovieListing("Casablanca",1942);
		temp.setKeyVector(none);
		tester.findSimilarity(temp);
	}
	
	
	@Test(expected = ListingException.class)
	public void exception_findsimilarity_inputNull() throws ListingException{
		MovieListing test_l=null;
		try{
			tester.findSimilarity(test_l);
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}		
	}
	
	
	
	
	@Test(expected = ListingException.class)
	public void exception_findsimilarity_keyVector() throws ListingException{	
		MovieListing tester_null = new MovieListing("Casablanca",1942);
		tester_null.setKeyVector(null);
		try{
			listings.get("CASABLANCA").findSimilarity(tester_null);
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}	
	}
	
	@Test(expected = ListingException.class)
	public void exception_getKeyVector() throws ListingException{
		
		try{
			tester.setKeyVector(null);
			tester.getKeyVector();
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	public void emptyReturn_getKeyVector() throws ListingException{
		BitSet empty = new BitSet();
		tester.setKeyVector(empty);
		assertEquals(empty,tester.getKeyVector());	
	}
	
	@Test
	public void normal_getkeyVector() throws ListingException{
		tester.setKeyVector(bs);
		assertEquals(bs,tester.getKeyVector());
	}
	
	@Test(expected = ListingException.class)
	public void exception_writeKeyVector() throws ListingException{
		
		try{
			tester.setKeyVector(null);
			tester.writeKeyVector();
			}
		catch(ListingException e){
			System.out.println(e.getMessage());
			throw e;
		}	
	}
	
	@Test
	public void emptyReturn_getkeywords() throws ListingException{
		Set<String> kw = new HashSet<String>();
		assertEquals(kw,tester.getKeywords());	
	}

	@Test
	public void normal_getKeywords() throws ListingException{
		String kw1 ="test_string1";
		String kw2 ="test_string2";

		Set<String> kw = new HashSet<String>();
		kw.add(kw1);
		kw.add(kw2);
		
		tester.addKeyword(kw1);
		tester.addKeyword(kw2);
		
		assertEquals(kw,tester.getKeywords());
	}
	
	@Test
	public void normal_getTitle(){
		assertEquals("Casablanca",tester.getTitle());
	}
	
	@Test
	public void HasTwoSpaceInside_Title() throws ListingException{
		tester = new MovieListing("Title_has Two Space",1942);
		assertEquals("Title_has Two Space",tester.getTitle());	
	}
	
	@Test
	public void normal_getYear(){
		assertEquals(1942,tester.getYear());
	}
	

	
	@Test
	public void normal_numKeywords() throws FileNotFoundException, IOException, ListingException{
		String kw1 ="test_string1";
		String kw2 ="test_string2";
		String kw3="test_string3";
		tester.addKeyword(kw1);
		tester.addKeyword(kw2);
		tester.addKeyword(kw3);	
		assertEquals(3,tester.numKeywords());
		
	}
	
	@Test
	public void empty_numKeywords() throws ListingException{

		assertEquals(0,tester.numKeywords());
		
	}
	
	@Test
	public void empty_setKeyVector() throws ListingException{
		BitSet empty = new BitSet();
		tester.setKeyVector(empty);
		assertEquals(empty,tester.getKeyVector());
		
	}
	
	@Test
	public void normal_setKeyVector() throws ListingException{
		tester.setKeyVector(bs);
		assertEquals(bs,tester.getKeyVector());	
	}
	
	@Test
	public void normal_writeKeyVector() throws ListingException{
		tester.setKeyVector(bs);
		assertEquals("{4, 6, 9, 10, 13, 16}",tester.writeKeyVector());
	}
	
	@Test
	public void empty_writeKeyVecotr() throws ListingException{
		BitSet empty = new BitSet();
		tester.setKeyVector(empty);
		assertEquals("{}",tester.writeKeyVector());
	}
	
	@Test
	public void writeKeywords(){
		assertEquals("year-1941:nazi:vichy:1940s:underground:world-war-two-in-africa:",
					listings.get("CASABLANCA").writeKeywords());
	}

}
