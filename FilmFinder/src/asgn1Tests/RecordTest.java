package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Index.IndexException;
import asgn1Index.Record;

public class RecordTest {
	private Record recordTester;
	
	@Before
	public void setup() throws IndexException{
	recordTester = new Record("test",2);		
	}
	
	@Test(expected = IndexException.class)
	public void exception_constructor_null() throws Exception{
		recordTester = new Record(null,2);	
	}
	
	@Test(expected = IndexException.class)
	public void exception_construcutor_empty() throws Exception{
		recordTester = new Record("",2);	
	}
	
	@Test(expected = IndexException.class)
	public void exception_construcutor_lessThan() throws Exception{
		recordTester = new Record("test",-1);	
	}
	
	@Test
	public void normal_construcutor() throws IndexException{
		recordTester = new Record("test1",3);
		assertEquals(3,recordTester.getSimilarity());
		assertEquals("test1",recordTester.getTitle());
	} 
	
	@Test
	public void test_compareTo_more() throws IndexException{
		Record new_record= new Record("test_text",1);
		assertTrue(recordTester.compareTo(new_record)>0);
	}
	
	@Test
	public void test_compareTo_less() throws IndexException{
		Record new_record= new Record("test_text",3);
		assertTrue(recordTester.compareTo(new_record)<0);
	}
	
	@Test
	public void test_compareTo_equal() throws IndexException{
		Record new_record= new Record("test_text",2);
		assertTrue(recordTester.compareTo(new_record)==0);
	}
	
	@Test
	public void normal_getSimilarity(){
		assertEquals(2,recordTester.getSimilarity());
		
	}
	@Test
	public void Zero_getSimilarity() throws IndexException{
		Record new_record= new Record("test_3",0);
		assertEquals(0,new_record.getSimilarity());
	}
	
	@Test
	public void normal_getTitle(){
		assertEquals("test",recordTester.getTitle());
		
	}
	
}


