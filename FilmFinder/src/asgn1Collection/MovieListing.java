package asgn1Collection;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class MovieListing implements Listing{
	private String title;
	private int year;
	private Set<String> keywords= new HashSet<String>();
	private BitSet keyVector = new BitSet();

	public MovieListing(String title, int year)throws ListingException{
		if(title==null){
	        throw new ListingException("title should not be null.");
		}else if(title==""){
			throw new ListingException("title should not be empty.");	
	    }
		this.title=title;
		
		if(year<=0){
			throw new ListingException("year should be more than 0.");
		}else{
			this.year=year;
		}
		
	}
	

	@Override
	public void addKeyword(String kw) throws ListingException {
		if(kw==null){
			throw new ListingException("keyword is null");
			}
		if(kw.isEmpty()){
			throw new ListingException("keyword isEmpty");		
		}
		
		this.keywords.add(kw);
	}

	@Override
	public int findSimilarity(Listing l) throws ListingException {
		if(this.keyVector==null){
			throw new ListingException("listing's keyVector is null");
			
		}
		if(l==null){
			throw new ListingException("input Listing l is null");
		}
		
		BitSet similarity = (BitSet) this.getKeyVector().clone();
		similarity.and(l.getKeyVector());
		return similarity.cardinality();
	}

	@Override
	public BitSet getKeyVector() throws ListingException {
		if(keyVector==null){
			throw new ListingException("KeyVector is null");
		}
		return keyVector;
	}

	@Override
	public Set<String> getKeywords() {
		return keywords;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public int getYear() {
		return year;
	}

	@Override
	public int numKeywords() {
		return keywords.size();
	}

	@Override
	public void setKeyVector(BitSet bs) {
		this.keyVector=bs;	
	}

	@Override
	public String writeKeyVector() throws ListingException {
		if(keyVector==null){
			throw new ListingException("keyVector is null");	
		}
		return keyVector.toString();
	}

	/* (non-Javadoc)
	 * @see asgn1Collection.Listing#writeKeywords()
	 */
	@Override
	public String writeKeywords() {
		String str=""; int index=0;
		for (String kw : this.keywords) {
			str += kw +":"; 
			index++;
			if ((index % 10)==0) {
				str += "\n";
			}
		}
		return str;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.title + ":" + this.year + ":" + "Active keywords:" + this.numKeywords();
	}
}
