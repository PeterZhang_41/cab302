/**
 * 
 */
package asgn1Index;

import asgn1Index.AbstractRecord;

/**
 * @author Peter
 * @param <T>
 *
 */
public class Record extends AbstractRecord implements Comparable<Record> {


	/**
	 * 
	 */
	public Record(String title, int similarity) throws IndexException{
		if(title ==null || title.isEmpty()||similarity<0 ){
			throw new IndexException("title is null or empty; Similarity should not be less than 0.");	
		}
		this.title= title;
		this.similarity=similarity;
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecord#getSimilarity()
	 */
	@Override
	public int getSimilarity() {
		// TODO Auto-generated method stub
		return similarity;
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecord#getTitle()
	 */
	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	public int compareTo(Record r) {
		if (r.similarity>this.similarity){
			return -1;
			
		}
		if (r.similarity==this.similarity){
			return 0;
			
		}
		
		return 1;
		
	}
	
}