/**
 * 
 */
package asgn1Index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Peter
 *
 */
public class RecordCollection extends AbstractRecordCollection {
	
	/**
	 * 
	 */
	public RecordCollection() {
		records = new ArrayList<Record>();
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecordCollection#addRecord(asgn1Index.Record)
	 */
	@Override
	public void addRecord(Record r) throws IndexException {
		if(r!=null){
			
			records.add(r);
			sorted = false;
		}else{
			
			throw new IndexException("record is null");
		}
		

	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecordCollection#findClosestRecord()
	 */
	@Override
	public AbstractRecord findClosestRecord() throws IndexException {
		if(sorted==false){
			throw new IndexException("collection has not yet sorted.");
		}
		if(records.size()==0){
			throw new IndexException("collection's size is zero");
			
		}
		return records.get(0);
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecordCollection#findClosestRecords(int)
	 */
	@Override
	public List<Record> findClosestRecords(int n) throws IndexException {
	
		if(sorted==false){
			throw new IndexException("collection has not yet sorted.");
		}
		if(n>records.size()){
			throw new IndexException("searching record is more than the size in collection");
			
		}
	
		List<Record> record = new ArrayList<Record>();
		
		for(int i=0;i<n;i++){
		record.add(records.get(i));
		}

		return record;
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecordCollection#isSorted()
	 */
	@Override
	public boolean isSorted() {
		
		return this.sorted;
	}

	/* (non-Javadoc)
	 * @see asgn1Index.AbstractRecordCollection#sortCollection()
	 */
	@Override
	public void sortCollection() {
		Collections.sort(records,Collections.reverseOrder());
		sorted = true;
	}

}
